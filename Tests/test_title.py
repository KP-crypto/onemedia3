from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import pytest
import time

def test_title_web_page():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get("https://yahoo.co.in")
    driver.maximize_window()
    title = driver.title
    assert "Yahoo Search - Web Search" in title
    driver.quit()
